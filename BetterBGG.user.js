﻿// ==UserScript==
// @name        BetterBGG
// @namespace   jcpmcdonald
// @description Better BoardGameGeek
// @include     /^http(s)?://(www\.)?boardgamegeek\.com/.*$/
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js
// @version     5.0
// @updateURL   https://bitbucket.org/jcpmcdonald/betterbgg/downloads/BetterBGG.meta.js
// @downloadURL https://bitbucket.org/jcpmcdonald/betterbgg/downloads/BetterBGG.user.js
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_listValues
// @grant       GM_xmlhttpRequest
// ==/UserScript==


/*#########################################################################
This script adds numerous features to BoardGameGeek.com (BGG)

1) Adds standard deviation (SD) beside the rating on a game's description. > 1.6 typically means it's a cult game
2) When you first view a game, it expands the "User Suggested # of Players" poll results. This is useful on its own, but there's more:
3) In all list views, BetterBGG will display cached standard deviations and suggested player counts so you can easily access this information at a glance
4) Caches the names of games that designers have designed, then displays those games on every other game they've designed.

Designed and written by John McDonald: bgg user jcpmcdonald 
https://boardgamegeek.com/user/jcpmcdonald

#########################################################################*/

$.noConflict();

if (window.top != window.self)  //don't run on frames or iframes
{
	return;
}

// Inject jQuery into the page for simpler debugging
//jQuery('head').append('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>');

// Reduce the "page jump" when the banner ad loads
jQuery('#dfp-leaderboard').css('height', '90px');


// Create a startsWith function
if (typeof String.prototype.startsWith != 'function') {
	String.prototype.startsWith = function (str) {
		return this.slice(0, str.length) == str;
	};
}

function getParameterByName(url, name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(url);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var isGM = true;
try{
	//console.log(GM_getValue.length);
	//isGM = !(typeof GM_getValue === "undefined" || GM_getValue("a", "b") === undefined);
	isGM = (GM_getValue.length > 0);
}catch(err){
	isGM = true;
}
//console.log("isGM = " + isGM);

/**
 * If we're running on a content page, this variable will point at an object
 * containing settings retrieved from the extension's localStorage, otherwise
 * we're running in the extension's context and want to access localStorage
 * directly.
 *
 * This allows us to include this script for use as a library in extension
 * contexts, such as in a prefs page.
 */
var cachedSettings = null;

if (!isGM)
{
    GM_getValue = function(name, defaultValue)
    {
        var value = (cachedSettings === null ?
                     localStorage.getItem(name) :
                     cachedSettings[name]);
        if (value === undefined || value === null)
        {
            return defaultValue;
        }
        
        var type = value[0];
        // var type = value[0],
            // value.substring(1);
        value = value.substring(1);
        switch (type)
        {
            case "b":
                return (value === "true");
            case "n":
                return Number(value);
            default:
                return value;
        }
    }

    GM_setValue = function(name, value)
    {
        value = (typeof value)[0] + value;
        if (cachedSettings === null)
        {
            localStorage.setItem(name, value);
        }
        else
        {
            cachedSettings[name] = value;
            chrome.extension.sendRequest({type: "setpref", name: name, value: value});
        }
    }
}


//#########################################################################
//#########################################################################
//#### Helper class that scrapes and inserts into the board game page
//#########################################################################
//#########################################################################
var boardgamePage = {
	
	isBetaUI: function () {
		return document.getElementById('mainbody') != null;
	},
	
	getBoardGameID: function () {
		if(boardgamePage.isBetaUI()){
			return jQuery('.game-title a').attr('href').split('/')[2];
		}else{
			var metaURLSplit = jQuery('html>head>meta[name|="og:url"]').attr('content').split('/');
			return metaURLSplit[metaURLSplit.length - 1];
		}
	},
	
	getPrimaryName: function() {
		if(boardgamePage.isBetaUI()){
			// or geekitem.name
			return jQuery('.game-title a').text().trim();
		}else{
			return jQuery(".geekitem_title > a > span").text();
		}
	},
	
	getBoardGameRank: function() {
		if(boardgamePage.isBetaUI()){
			// or geekitem.rankinfo[0].rank where [0].rankobjectid = 1, or [0].veryshortprettyname = "Overall"
			return parseInt(jQuery('dl[title="Board Game Rank"] .rank-value').text());
		} else {
			return parseInt(jQuery('td.sf > div.mf:nth(0) > a').text());
		}
	},
	
	
	gatherMainInfoBetaUI: function(boardGameInfo) {
		
		var geekitem = unsafeWindow.angular.element('.game').inheritedData().$scope.geekitemctrl.geekitem.data.item;
		var forEach = Array.prototype.forEach;
		
		var debug = 0;
		
		//console.log('getting data');
		
		// Designers
		boardGameInfo.designers = [];
		boardGameInfo.designerURLs = [];
		forEach.call(geekitem.links.boardgamedesigner, function(e){
			boardGameInfo.designers.push(e.name);
			boardGameInfo.designerURLs.push(e.href);
		});
		
		//console.log(debug++);
		
		// Artists
		boardGameInfo.artists = [];
		boardGameInfo.artistURLs = [];
		forEach.call(geekitem.links.boardgameartist, function(e, i){
			boardGameInfo.artists.push(e.name);
			boardGameInfo.artistURLs.push(e.href);
		});
		
		//console.log(debug++);
		
		// Publishers
		boardGameInfo.publishers = [];
		boardGameInfo.publisherURLs = [];
		forEach.call(geekitem.links.boardgamepublisher, function(e, i){
			boardGameInfo.publishers.push(e.name);
			boardGameInfo.publisherURLs.push(e.href);
		});
		
		//console.log(debug++);
		
		// // Honours
		// angular.element('.game-awards .panel-body').inheritedData().$ngControllerController.honors
		var honoursElement = unsafeWindow.angular.element('.game-awards .panel-body');
		if(honoursElement && honoursElement.length > 0){
			var honourCtrl = honoursElement.inheritedData();
			boardGameInfo.honours = [];
			forEach.call(honourCtrl.$ngControllerController.honors, function(e, i){
					//console.log(e);
					boardGameInfo.honours.push({
						name: e.name,
						href: e.href
					});
			});
		}
		
		//console.log(debug++);
		
		// Categories
		boardGameInfo.categories = [];
		boardGameInfo.categoryURLs = [];
		forEach.call(geekitem.links.boardgamecategory, function(e, i){
			boardGameInfo.categories.push(e.name);
			boardGameInfo.categoryURLs.push(e.href);
		});
		
		//console.log(debug++);
		
		// mechanics
		boardGameInfo.mechanics = [];
		boardGameInfo.mechanicURLs = [];
		forEach.call(geekitem.links.boardgamemechanic, function(e, i){
			boardGameInfo.mechanics.push(e.name);
			boardGameInfo.mechanicURLs.push(e.href);
		});
		
		//console.log(debug++);
		
		// Expansion
		// angular.element('linked-items-module').inheritedData()
		// https://boardgamegeek.com/api/geekitem/linkeditems?ajax=1&linkdata_index=boardgameexpansion&objectid=9209&objecttype=thing&pageid=2&showcount=10&subtype=boardgameexpansion
		// boardGameInfo.expansions = [];
		// boardGameInfo.expansionURLs = [];
		// forEach.call(geekitem.links.boardgameexpansion, function(e, i){
			// boardGameInfo.expansions.push(e.name);
			// boardGameInfo.expansionURLs.push(e.href);
		// });
		
		// Expands
		boardGameInfo.expands = [];
		boardGameInfo.expandsURLs = [];
		forEach.call(geekitem.links.expandsboardgame, function(e, i){
			boardGameInfo.isExpansion = true;
			boardGameInfo.expands.push(e.name);
			boardGameInfo.expandsURLs.push(e.href);
		});
		
		//console.log(debug++);
		
		// Integrates With
		boardGameInfo.integratesWith = [];
		forEach.call(geekitem.links.boardgameintegration, function(e, i){
			boardGameInfo.integratesWith.push({
				name: e.name,
				href: e.href
			});
		});
		
		//console.log(debug++);
		
		// Reimplemented By
		boardGameInfo.reimplementedBy = [];
		boardGameInfo.reimplementedByURLs = [];
		forEach.call(geekitem.links.reimplementation, function(e, i){
			boardGameInfo.reimplementedBy.push(e.name);
			boardGameInfo.reimplementedByURLs.push(e.href);
		});
		
		//console.log(debug++);
		
		// Reimplements
		boardGameInfo.reimplements = [];
		forEach.call(geekitem.links.reimplements, function(e, i){
			boardGameInfo.reimplements.push({
				name: e.name,
				href: e.href
			});
		});
		
		//console.log(debug++);
		
		// Family
		boardGameInfo.families = [];
		boardGameInfo.familyURLs = [];
		forEach.call(geekitem.links.boardgamefamily, function(e, i){
			boardGameInfo.families.push(e.name);
			boardGameInfo.familyURLs.push(e.href);
		});
		
		//console.log(debug++);
		
		
		// contains
		// containedin
		// videogamebg
		// boardgamesubdomain
		// boardgameaccessory
		
		
		// handled elsewhere
		//geekitem.name
		
		boardGameInfo.alternateNames = [];
		forEach.call(geekitem.alternatenames, function(e, i){
			boardGameInfo.alternateNames.push(e.name);
		});
		
		boardGameInfo.standardDeviation = Number(geekitem.stats.stddev);
		boardGameInfo.yearPublished = parseInt(geekitem.yearpublished);
		boardGameInfo.minplayers = parseInt(geekitem.minplayers);
		boardGameInfo.maxplayers = parseInt(geekitem.maxplayers);
		boardGameInfo.minplaytime = parseInt(geekitem.minplaytime);
		boardGameInfo.maxplaytime = parseInt(geekitem.maxplaytime);
		boardGameInfo.minage = parseInt(geekitem.minage);
		boardGameInfo.website = geekitem.website.url;
		
		//console.log(debug++);
		
		// Wipe Deprecated fields
		//delete boardGameInfo.mfgSuggestedAges;
		//delete boardGameInfo.playingTime;
		
		//console.log(boardGameInfo);
	},
	
	
	gatherMainInfo: function(boardGameInfo) {
		// Go through each row of the main data table
		jQuery('div[id^=results] > a[name=information]').parent().find('.innermoduletable .geekitem_infotable > tbody > tr').each(function(i, e){
			// Then do something special depending on the row
			var label = jQuery('td:first > b', e).text();
			
			switch(label){
			case "Designer":
				boardGameInfo.designers = [];
				boardGameInfo.designerURLs = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.designers.push(jQuery(e).text());
					boardGameInfo.designerURLs.push(jQuery(e).attr("href"));
				});
				// The remaining * are hidden, but still there
				jQuery('td:nth-child(2) > div > div > div > a', e).each(function(i, e){
					boardGameInfo.designers.push(jQuery(e).text());
					boardGameInfo.designerURLs.push(jQuery(e).attr("href"));
				});
				break;
			case "Artist":
				boardGameInfo.artists = [];
				boardGameInfo.artistURLs = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.artists.push(jQuery(e).text());
					boardGameInfo.artistURLs.push(jQuery(e).attr("href"));
				});
				jQuery('td:nth-child(2) > div > div > div > a', e).each(function(i, e){
					boardGameInfo.artists.push(jQuery(e).text());
					boardGameInfo.artistURLs.push(jQuery(e).attr("href"));
				});
				break;
			case "Publisher":
				boardGameInfo.publishers = [];
				boardGameInfo.publisherURLs = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.publishers.push(jQuery(e).text());
					boardGameInfo.publisherURLs.push(jQuery(e).attr("href"));
				});
				jQuery('td:nth-child(2) > div > div > div > a', e).each(function(i, e){
					boardGameInfo.publishers.push(jQuery(e).text());
					boardGameInfo.publisherURLs.push(jQuery(e).attr("href"));
				});
				break;
			case "Year Published":
				boardGameInfo.yearPublished = parseInt(jQuery('td:nth-child(2) > div > div:nth-child(2)', e).text());
				break;
			case "# of Players":
				// Handled elsewhere
				break;
			case "User Suggested # of Players":
				// Handled elsewhere
				break;
			case "Mfg Suggested Ages":
				boardGameInfo.mfgSuggestedAges = parseInt(jQuery('td:nth-child(2)', e).text());
				break;
			case "Playing Time":
				// Formats are:
				// 180 minutes
				// 60 − 120 minutes
				var playTimeEnglish = jQuery('td:nth-child(2)', e).text().trim();
				
				// Make sure it ends with " minutes", then trim it
				if(playTimeEnglish.endsWith(" minutes")){
					playTimeEnglish = playTimeEnglish.substring(0, playTimeEnglish.length - 8);
					var split = playTimeEnglish.split("−");
					boardGameInfo.minplaytime = split[0];
					boardGameInfo.maxplaytime = split[split.length - 1];	// Last
				}
				break;
			case "User Suggested Ages":
				// A bit hard to do fully, but I guess we can grab the summary
				boardGameInfo.userSuggestedAges = parseInt(jQuery('td:nth-child(2)', e).text());
				break;
			case "Language Dependence":
				boardGameInfo.languageDependence = jQuery('td:nth-child(2) > div', e)[0].childNodes[0].nodeValue.trim();
				break;
			case "Honors":
				break;
			case "Subdomain":
				boardGameInfo.subdomain = jQuery('td:nth-child(2) > div > div:nth-child(1)', e).text().trim();
				break;
			case "Category":
				boardGameInfo.categories = [];
				boardGameInfo.categoryURLs = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.categories.push(jQuery(e).text());
					boardGameInfo.categoryURLs.push(jQuery(e).attr("href"));
				});
				jQuery('td:nth-child(2) > div > div > div > a', e).each(function(i, e){
					boardGameInfo.categories.push(jQuery(e).text());
					boardGameInfo.categoryURLs.push(jQuery(e).attr("href"));
				});
				break;
			case "Mechanic":
				boardGameInfo.mechanics = [];
				boardGameInfo.mechanicURLs = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.mechanics.push(jQuery(e).text());
					boardGameInfo.mechanicURLs.push(jQuery(e).attr("href"));
				});
				jQuery('td:nth-child(2) > div > div > div > a', e).each(function(i, e){
					boardGameInfo.mechanics.push(jQuery(e).text());
					boardGameInfo.mechanicURLs.push(jQuery(e).attr("href"));
				});
				break;
			case "Expansion":
				boardGameInfo.expansions = [];
				boardGameInfo.expansionURLs = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.expansions.push(jQuery(e).text());
					boardGameInfo.expansionURLs.push(jQuery(e).attr("href"));
				});
				jQuery('td:nth-child(2) > div > div > div > a', e).each(function(i, e){
					boardGameInfo.expansions.push(jQuery(e).text());
					boardGameInfo.expansionURLs.push(jQuery(e).attr("href"));
				});
				break;
			case "Expands":
				boardGameInfo.isExpansion = true;
				boardGameInfo.expands = [];
				boardGameInfo.expandsURLs = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.expands.push(jQuery(e).text());
					boardGameInfo.expandsURLs.push(jQuery(e).attr("href"));
				});
				jQuery('td:nth-child(2) > div > div > div > a', e).each(function(i, e){
					boardGameInfo.expands.push(jQuery(e).text());
					boardGameInfo.expandsURLs.push(jQuery(e).attr("href"));
				});
				break;
			case "Reimplemented By":
				boardGameInfo.reimplementedBy = [];
				boardGameInfo.reimplementedByURLs = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.reimplementedBy.push(jQuery(e).text());
					boardGameInfo.reimplementedByURLs.push(jQuery(e).attr("href"));
				});
				jQuery('td:nth-child(2) > div > div > div > a', e).each(function(i, e){
					boardGameInfo.reimplementedBy.push(jQuery(e).text());
					boardGameInfo.reimplementedByURLs.push(jQuery(e).attr("href"));
				});
				break;
			case "Family":
				boardGameInfo.families = [];
				boardGameInfo.familyURLs = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.families.push(jQuery(e).text());
					boardGameInfo.familyURLs.push(jQuery(e).attr("href"));
				});
				jQuery('td:nth-child(2) > div > div > div > a', e).each(function(i, e){
					boardGameInfo.families.push(jQuery(e).text());
					boardGameInfo.familyURLs.push(jQuery(e).attr("href"));
				});
				break;
			case "Primary Name":
				// Handled elsewhere
				//boardGameInfo.name = jQuery('td:nth-child(2)', e).text();
				break;
			case "Alternate Names":
				boardGameInfo.alternateNames = [];
				jQuery('td:nth-child(2) > div > div > a', e).each(function(i, e){
					boardGameInfo.alternateNames.push(jQuery(e).text().trim());
				});
				jQuery('td:nth-child(2) > div > div > div', e).each(function(i, e){
					boardGameInfo.alternateNames.push(jQuery(e).text().trim());
				});
				break;
			case "Website":
				var website = jQuery('td:nth-child(2) > div > div > div > a', e).attr("href");
				//console.log(website);
				if(website != null && website != "" && typeof(website) !== 'undefined' ){
					boardGameInfo.website = website;
				}
				break;
			}
		});
		return boardGameInfo;
	},
	
	getStandardDeviation: function() {
		if(boardgamePage.isBetaUI()){
			
			var geekitem = unsafeWindow.angular.element('.game').inheritedData().$scope.geekitemctrl.geekitem.data.item;
			var stddev = geekitem.stats.stddev;
			//console.log("stddev = " + stddev);
			return stddev;
		}else{
			return Number(jQuery('div[id^=results] > a[name=statistics]').parent().find('.innermoduletable table:eq(0) tr:contains("Deviation") td:eq(1)').text());
		}
	},


	getPlayerCountPollResultsButton: function(boardGameID, callback) {
		// OLD UI ONLY
		jQuery.ajax({
			url: '/geekitempoll.php?action=view&itempolltype=numplayers&objecttype=thing&objectid=' + boardGameID,
			success: function(data) {
				//console.log(data);
				data = jQuery('<div/>').append(data);
				var resultsButton = data.find('#clearselection+ input');
				var js = resultsButton.attr('onclick');
				resultsButton.attr('id', 'playerCountResultsButton');

				var pollID = Number(js.match(/\d+/g));
				
				callback(pollID, resultsButton);
			}
		});
	},

	getPlayerPollResults: function(pollID, boardGameInfo) {
		// NEW UI ONLY
		jQuery.ajax({
			headers: {
				Accept: 'application/json, text/plain, */*'
			},
			url: '/geekpoll.php?action=results&pollid=' + pollID,
			success: function(data) {
				data = JSON.parse(data);
				//console.log(data);
				var suggestedPlayerCount = Array();
				
				// Figure out how many players were voted on
				var maxPlayers = data.pollquestions[0].results.question.choicesr.length - 1;
				
				var index;
				for(index = 0; index <= maxPlayers + 1; index++)
				{
					suggestedPlayerCount[index] = {
						best: -1,
						good: -1,
						bad: -1
					};
				}
				
				data.pollquestions[0].results.results.forEach(function(result){
					var playerCount = parseInt(result.rowbody);
					if(playerCount <= maxPlayers + 1)
					{
						if(result.columnbody == "Best"){
							suggestedPlayerCount[playerCount].best = parseInt(result.votes);
						}else if(result.columnbody == "Recommended"){
							suggestedPlayerCount[playerCount].good = parseInt(result.votes);
						}else if(result.columnbody == "Not Recommended"){
							suggestedPlayerCount[playerCount].bad = parseInt(result.votes);
						}
					}
					//suggestedPlayerCount[playerCountForRow].best = Number(matches[1]);
				});
				
				boardGameInfo.maxPlayers = maxPlayers;
				boardGameInfo.suggestedPlayerCount = suggestedPlayerCount;
				GM_setValue(boardGameInfo.boardGameID, JSON.stringify(boardGameInfo));
				
				boardgamePage.injectPlayerCountPollGraph(boardGameInfo);
			}
		});
	},
	
	
	injectPlayerCountPollGraph: function(boardGameInfo){
		// Beta UI only
		if(!boardgamePage.isBetaUI()){ console.log('Beta UI only'); return; }
		
		var bestWithPlayersHTML = "<p>";
				
		for(index = 1; index <= boardGameInfo.maxPlayers; index++)
		{
			var totalVotes = boardGameInfo.suggestedPlayerCount[index].best + boardGameInfo.suggestedPlayerCount[index].good + boardGameInfo.suggestedPlayerCount[index].bad;
			var bestPercent = (boardGameInfo.suggestedPlayerCount[index].best / totalVotes) * 100;
			var goodPercent = (boardGameInfo.suggestedPlayerCount[index].good / totalVotes) * 100;
			var badPercent = (boardGameInfo.suggestedPlayerCount[index].bad / totalVotes) * 100;
			
			bestWithPlayersHTML += "<span style='display: inline-flex; width: 100%' class='suggestedPlayerRow' data-toggle='tooltip' data-placement='top' title='Best: " + bestPercent.toFixed(1) + "%, Recommended: " + goodPercent.toFixed(1) + "%, Not Recommended: " + badPercent.toFixed(1) + "%'>" + 
			"<span style='display: inline; width: 20px; text-align: right;'>" + index + "</span>" +
				"<span style='display: inline; width:" + bestPercent + "%; background-color:green; margin: 2px 0px 2px 2px;'></span>" + 
				"<span style='display: inline; width:" + goodPercent + "%; background-color:yellow; margin: 2px 0px 2px 0px;'></span>" + 
				"<span style='display: inline; width:" + badPercent + "%; background-color:red; margin: 2px 0px 2px 0px;'></span>" + 
				"</span><br/>";
		}
		
		bestWithPlayersHTML += "</p>";
		jQuery(jQuery.parseHTML(bestWithPlayersHTML)).appendTo(jQuery(".feature:eq(2)"));
		
		window.eval("jQuery('.suggestedPlayerRow').tooltip();");
	},
	
	
	appendStandardDeviation: function(standardDeviation) {
		jQuery('.geekitem_title').parent().parent().find('td:nth-child(1) .b').append('(SD:' + standardDeviation.toFixed(2) + ')');
	},
	
	appendDesignerGames: function(boardGameInfo){
		var allDesigners = JSON.parse(GM_getValue("designers"));
		
		if(boardgamePage.isBetaUI())
		{
			jQuery('li:nth-child(1) .ng-scope .ng-binding a').each(function(i, e){
				var designer = jQuery(e).text();
				//console.log(designer);
				//console.log(e);
				//console.log(jQuery(e).parent());
				boardgamePage.appendDesignerGamesIntoElement(boardGameInfo, designer, allDesigners, jQuery(e).parent());
			});
		}
		else
		{
			// For each designer on page
			jQuery('div[id^=results] > a[name=information]').parent().find('.innermoduletable .geekitem_infotable > tbody > tr:nth-child(1) > td:nth-child(2) > div > div > a').each(function(i, e){
				var designer = jQuery(e).text();
				boardgamePage.appendDesignerGamesIntoElement(boardGameInfo, designer, allDesigners, jQuery(e).parent());
			});
		}
	},
	
	appendDesignerGamesIntoElement: function(boardGameInfo, designerName, allDesigners, element){
		// Display the "also designed" section IF this designer has designed multiple games, OR designed one game that's not this one
		//console.log(designerName);
		//console.log(allDesigners[designerName]);
		if(typeof(allDesigners[designerName]) !== 'undefined' && (allDesigners[designerName].length > 1 || allDesigners[designerName][0] != boardGameInfo.boardGameID)){
			var alsoDesigned = "&nbsp;&nbsp;<span style='font-size: smaller'>(also designed: ";
			
			// Look up all the (cached) games this designer has designed
			var firstInList = true;
			allDesigners[designerName].forEach(function(designedBoardGameID){
				if(designedBoardGameID != boardGameInfo.boardGameID){
					var designedGame = JSON.parse(GM_getValue(designedBoardGameID));
					
					if(!firstInList)
					{
						alsoDesigned += ', ';
					}
					firstInList = false;
					
					alsoDesigned += "<a href='http://boardgamegeek.com/boardgame/" + designedBoardGameID + "'>" + designedGame.name + "</a>";
				}
			});
			
			alsoDesigned += ")</span>";
			jQuery(jQuery.parseHTML(alsoDesigned)).appendTo(element);
		}
	},
	
	
	appendPlayCountPollResults: function(boardGameID, pollID, resultsButton, boardGameInfo) {
		var landingDiv = jQuery('#numplayers_' + boardGameID);
		//js = js.replace('this', "'#numplayers_ + boardGameID);
		//resultsButton.attr('onclick', js);


		//jQuery('head').append("<script>" + GP_ViewResults + GP_SetResponse + "</script>");

		landingDiv.append('<div class="outerpoll"><div id="status_p' + pollID + '"></div><div class="pollquestions' + pollID + '" id="pollquestions' + pollID + '"></div></div>');
		jQuery('.pollquestions' + pollID).append(resultsButton);
		jQuery('#playerCountResultsButton')[0].addEventListener("click", function(){ boardgamePage.capturePlayerCountPoll(pollID, boardGameInfo); } );
		//jQuery('#numplayers_' + boardGameID).show();
		
		if(typeof boardGameInfo.suggestedPlayerCount === 'undefined' ||  boardGameInfo.suggestedPlayerCount == null){
			// Click it?
			// yeah, click it
			jQuery('#playerCountResultsButton')[0].click();
		}
		
		//callback(votes);
	},
	
	capturePlayerCountPoll: function(pollID, boardGameInfo){
		// This only works with the OLD UI, not the beta
		// The button for poll results was clicked. Try to find the results table
		var playerCountInterval = setInterval(function(){
				var pollAnswerTable = jQuery('#pollquestions' + pollID + ' .pollresults');
				if(typeof pollAnswerTable[0] !== 'undefined'){
					//console.log("BetterBGG Capturing Player Count Poll");
					clearInterval(playerCountInterval);
					
					//console.log(pollAnswerTable.find("tbody>tr"));
					
					var suggestedPlayerCount = Array();
					var playerCountForRow;
					
					pollAnswerTable.find("tbody>tr").each(function(i, row){
						playerCountForRow = -1;
						jQuery(row).find("td").each(function(j, c){
							var col = jQuery(c);
							//console.log(col.text().trim());
							if(j == 0 && col.text().trim().indexOf('more than') != 0){
								playerCountForRow = Number(col.text());
								suggestedPlayerCount[playerCountForRow] = {
									best: -1,
									good: -1,
									bad: -1
								};
							}else if(playerCountForRow > 0 && j >= 1 && j <= 3){
								var regExp = /\(([^)]+)\)/;
								var matches = regExp.exec(col.text());
								
								//matches[1] contains the value between the parentheses
								//console.log(matches[1]);
								
								if(j == 1){
									suggestedPlayerCount[playerCountForRow].best = Number(matches[1]);
								}else if(j == 2){
									suggestedPlayerCount[playerCountForRow].good = Number(matches[1]);
								}else if(j == 3){
									suggestedPlayerCount[playerCountForRow].bad = Number(matches[1]);
								}
							}
							
						});
					});
					
					//console.log(suggestedPlayerCount);
					boardGameInfo.suggestedPlayerCount = suggestedPlayerCount;
					GM_setValue(boardGameInfo.boardGameID, JSON.stringify(boardGameInfo));
				}
			}
			, 1500);
	}
};



function copyPlay()
{
	jQuery('#copyPlay').remove();
	jQuery('#copyPlayResults').html('Copying...');
	
	var queryString = '&version=2&objecttype=thing';
	
	var gameLog = {
		playid: null,
		objectid: null,
		playdate: '2014-01-01',
		location: null,
		quantity: 1,
		length: null,
		incomplete: 0,
		nowinstats: 0,
		comments: "",
		players: null
	};
	
	// Collect the play information
	jQuery('.forum_table').each(function(idx){
		
		if(idx == 0)
		{
			jQuery(this).find('tr').each(function(row)
			{
				var txt = jQuery(this).children('td:eq(1)').text().trim();
				
				switch(row)
				{
				case 1:
					// Extract the game's ID from the link
					// eg: "/boardgame/2651/power-grid"
					//console.log(jQuery(this));
					var gameLink = jQuery(this).find('td > h2 > a').attr('href');
					gameLog.objectid = gameLink.split('/')[2];
					break;
					
				case 3:
					// Date
					gameLog.playdate = (txt == "N/A") ? null : txt;
					break;
					
				case 4:
					// Location
					gameLog.location = (txt == "N/A") ? null : txt;
					break;
					
				case 5:
					// Length
					gameLog.length = (txt == "N/A") ? null : txt;
					break;
					
				case 6:
					// Quantity
					gameLog.quantity = (txt == "N/A") ? null : Number(txt);
					break;
					
				}
			});
		}
		else if(idx == 1)
		{
			gameLog.players = getPlayers(jQuery(this));
		}
		else if(idx == 2)
		{
			gameLog.comments = jQuery(this).find('tr td').contents().filter(function() { return (this.nodeType === 3 && jQuery(this).text().trim() !== ''); }).text().trim();
			//console.log(gameLog.comment);
		}
		
	});
	
	//console.log(gameLog);
	//console.log(players);
	
	queryString += "&" + decodeURIComponent(jQuery.param(gameLog));
	
	//queryString += "&comments=" + encodeURIComponent(gameLog.comments);
	
	//console.log(queryString);
	
	// Create a function that I can inject into the page
	function doCopy(qs){
		new Request.JSON(
			{
				url:'/geekplay.php',
				data:'ajax=1&action=save' + qs,
				onComplete:	function(responseJSON,responseText)
				{
					//jQuery('#copyPlayResults').html(responseJSON.html);
					document.getElementById('copyPlayResults').innerHTML = responseJSON.html;
					
					// if(responseJSON.error)
					// {
						// //var error = new Element('div',{'html':responseJSON.error,'id':'quickplay_notification'+quickplayid,'class':'errorbox'});
						// //error.inject(results,'top');
						// (function(){error.set("tween",{duration:1500}).fade('out').get('tween').chain(function(){error.destroy()});
						// }).delay(5000);
					// }
					//saveData['plays'+quickplayid]['editactive'] = false;
				}
			}
		).send();
	}
	
	// Inject the function into the page, then call the function
	jQuery('head').append('<script>' + doCopy + ' doCopy("' + queryString + '");</script>');
	
}




function getPlayers(playerTable)
{
	var headerRow = true;
	var playerNum = 0;
	var players = [];
	
	playerTable.find('tr').each(function()
	{
		if(headerRow){ headerRow = false; return true; }
		
		players.push({
			name: null,
			username: null,
			new: 0,
			position: null,
			color: null,
			score: null,
			win: 0,
			rating: null
		});
		
		jQuery(this).children('td').each(function(index){
			switch(index)
			{
			case 0:
				// The first field may contain a username
				players[playerNum].name = jQuery(this).html().trim();
				var links = jQuery(this).children('a');
				if(links.length == 1)
				{
					players[playerNum].username = links.text();
					players[playerNum].name = players[playerNum].name.substring(0, players[playerNum].name.indexOf('(<')).trim();
				}
				break;
				
			case 1:
				if(jQuery(this).children().length > 0)
				{
					players[playerNum].new = 1;
				}
				break;
				
			case 2:
				if(jQuery(this).text().trim() != "N/A")
				{
					players[playerNum].position = jQuery(this).text().trim();
				}
				break;
				
			case 3:
				if(jQuery(this).text().trim() != "N/A")
				{
					players[playerNum].color = jQuery(this).text().trim();
				}
				break;
				
			case 4:
				if(jQuery(this).text().trim() != "N/A")
				{
					players[playerNum].score = jQuery(this).text().trim();
				}
				break;
				
			case 5:
				if(jQuery(this).children().length > 0)
				{
					players[playerNum].win = 1;
				}
				break;
				
			case 6:
				rating = jQuery(this).text().trim();
				break;
				
			}
		});
		
		
		playerNum = playerNum + 1;
	});
	
	return players;
}




function versionCheck(){
	switch(JSON.parse(GM_getValue("dataversion", JSON.stringify({ version: 3 }))).version)
	{
	case 1:
		console.log("Upgrading from database version 1 to 2");
		
		var designers = {};
		var artists = {};
		
		// Unfortunately, the following code does not filter out expansions, and there's no way to do it, so just start fresh
		// var keys = GM_listValues();
		// for (var i = 0, len = keys.length; i < len; i++)
		// {
			// //console.log(keys[i]);
			// //console.log(GM_getValue(keys[i]));
			// var currentGame = JSON.parse(GM_getValue(keys[i]));
			// 
			// if(typeof(currentGame.designers) !== 'undefined')
			// {
				// var designerNames = currentGame.designers;
				// designerNames.forEach(function(val, i){
					// //console.log(val + "\n");
					// //console.log(BGGData.designers.hasOwnProperty(val));
					// if(!designers.hasOwnProperty(val)){
						// designers[val] = [];
					// }
					// designers[val].push(currentGame.boardGameID);
				// });
			// }
			// 
			// if(typeof(currentGame.artists) !== 'undefined')
			// {
				// var artistNames = currentGame.artists;
				// artistNames.forEach(function(val, i){
					// //console.log(BGGData.artists.hasOwnProperty(val));
					// if(!artists.hasOwnProperty(val)){
						// artists[val] = [];
					// }
					// artists[val].push(currentGame.boardGameID);
				// });
			// }
		// }
		
		//console.log(designers);
		//console.log(artists);
		GM_setValue("designers", JSON.stringify(designers));
		GM_setValue("artists", JSON.stringify(artists));
		GM_setValue("dataversion", JSON.stringify({ version: 2 }));
		
	case 2:
		console.log("Upgrading from database version 2 to 3");
		var keys = GM_listValues();
		for (var i = 0, len = keys.length; i < len; i++)
		{
			var currentGame = JSON.parse(GM_getValue(keys[i]));
			delete currentGame["playingTime"];
			GM_setValue(keys[i], JSON.stringify(currentGame));
		}
		
		GM_setValue("dataversion", JSON.stringify({ version: 3 }));
		console.log("Upgrade complete");
	}
}

function init()
{
	//console.log('Init');
	versionCheck();
	//#########################################################################
	//#########################################################################
	//#### Figure out which page we are on and take the appropriate action
	//#########################################################################
	//#########################################################################
	if (window.location.pathname.startsWith("/betterbggconfig")) {
		jQuery('body').empty();
		
		
		
		// GM_xmlhttpRequest({
			// method: "GET",
			// url: "http://pastebin.com/raw.php?i=6unsmbHx",
			// headers: {
				// "User-Agent": "Mozilla/5.0",    // If not specified, navigator.userAgent will be used.
				// "Accept": "text/xml"            // If not specified, browser defaults will be used.
			// },
			// onload: function(response) {
				// jQuery('body').append(response.responseText);
			// }
		// });
		
		// var BGGData = {
			// games: [],
			// designers: {},
			// artists: {}
		// };
		var BGGData = {};
		
		var keys = GM_listValues();
		for (var i = 0, len = keys.length; i < len; i++)
		{
			//console.log(keys[i]);
			//console.log(GM_getValue(keys[i]));
			
			var gameID = GM_getValue(keys[i]);
			//console.log(GM_getValue(keys[i]));
			var currentGame = JSON.parse(GM_getValue(keys[i]));
			BGGData[keys[i]] = currentGame;
			
		}
		//console.log(BGGData);
		jQuery('body').append("<pre>" + JSON.stringify(BGGData, null, '\t') + "</pre>");
		
		//jQuery('body').append(GM_xmlhttpRequest("configHtml").responseText());
		//jQuery('body').append("<html><input type='text' id='txt' /><input type='button' id='myButton' /></html>");
		// jQuery('#myButton').on('click', function(){
			// //console.log("asdf");
			// console.log(jQuery("#txt").val());
		// });
	}
	else if (window.location.pathname.startsWith("/boardgame/") || window.location.pathname.startsWith("/boardgameexpansion/")) {
		var boardGameID = boardgamePage.getBoardGameID();
		
		//console.log(boardGameID);
	
		// Pull the cached information, or create a new instance
		var boardGameInfo = GM_getValue(boardGameID);
		if (typeof boardGameInfo !== "undefined" && boardGameInfo !== null) {
			boardGameInfo =  JSON.parse(boardGameInfo);
		}
		else
		{
			boardGameInfo = {
				lastUpdate: Date.now(),
				boardGameID: boardGameID,
				standardDeviation: -1,
				
				playerCountPollID: -1,
				
				suggestedPlayerCount: null
			};
		}
		//console.log(boardGameInfo);
		
		
		var designers = GM_getValue("designers");
		if (typeof designers !== "undefined" && designers !== null) {
			designers =  JSON.parse(designers);
		}
		else
		{
			// This should really never happen if the database upgrade occured correctly
			designers = {};
		}
		
		// Pull/update information
		boardGameInfo.name = boardgamePage.getPrimaryName();
		boardGameInfo.rank = boardgamePage.getBoardGameRank()
		
		if(boardgamePage.isBetaUI()){
			//console.log('beta');
			boardgamePage.gatherMainInfoBetaUI(boardGameInfo);
		}
		else
		{
			boardGameInfo.standardDeviation = boardgamePage.getStandardDeviation();
			boardgamePage.gatherMainInfo(boardGameInfo);
		}
		
		
		// If this is not an expansion, add this game to the designer's list of games for quick lookups
		if(!boardGameInfo.isExpansion){
			var designerNames = boardGameInfo.designers;
			designerNames.forEach(function(designer, i){
				// If the designer does not exist, make them
				if(!designers.hasOwnProperty(designer)){
					designers[designer] = [];
				}
				
				// If the game is not in their repertoire, add it
				if(jQuery.inArray(boardGameInfo.boardGameID, designers[designer]) == -1){
					designers[designer].push(boardGameInfo.boardGameID);
				}
			});
		}
		
		
		// Save
		boardGameInfo.lastUpdate = Date.now();
		GM_setValue(boardGameID, JSON.stringify(boardGameInfo));
		GM_setValue("designers", JSON.stringify(designers));
		
		
		if(boardgamePage.isBetaUI())
		{
			
			if(!boardGameInfo.playerCountPollID || boardGameInfo.playerCountPollID == -1){
				// New UI. Add player count poll results in-line as a little graph
				//console.log("new UI poll");
				// 1) Fetch the poll ID (using the game ID)
				jQuery.ajax({
					headers: {
						Accept: 'application/json, text/plain, */*'
					},
					url: '/geekitempoll.php?action=view&itempolltype=numplayers&objecttype=thing&objectid=' + boardGameID,
					success: function(data) {
						data = JSON.parse(data);
						//console.log(data);
						//console.log(data.poll.pollid);
						boardGameInfo.playerCountPollID = data.poll.pollid;
						GM_setValue(boardGameID, JSON.stringify(boardGameInfo));
						boardgamePage.getPlayerPollResults(boardGameInfo.playerCountPollID, boardGameInfo);
					}
				});
			}
			else
			{
				//console.log("Already have poll id: " + boardGameInfo.playerCountPollID);
				boardgamePage.getPlayerPollResults(boardGameInfo.playerCountPollID, boardGameInfo);
			}
			
		}
		else
		{
			// Old UI. Add a "Results" button to the player count poll area
			// Initiate insertion of player count poll results button
			// TODO: Do a better job of caching the button
			boardgamePage.getPlayerCountPollResultsButton(boardGameID, function(pollID, resultsButton) {
				boardGameInfo.playerCountPollID = pollID;
				boardgamePage.appendPlayCountPollResults(boardGameID, pollID, resultsButton, boardGameInfo);
		
				GM_setValue(boardGameID, JSON.stringify(boardGameInfo));
			});
		}
		
		
		// Display additional information from the cache
		boardgamePage.appendStandardDeviation(boardGameInfo.standardDeviation);
		boardgamePage.appendDesignerGames(boardGameInfo);
		
		
		
		/* This feature doesn't work very well as-is
		// Increase the size of the thumbnails
		var imageLoadInterval = setInterval(function(){
			var imageTable = jQuery('#results_5 .moduletable .pt5');
			if(typeof imageTable[0] !== 'undefined'){
				clearInterval(imageLoadInterval);
				
				jQuery('#results_5 .moduletable img').each(function(i, e){
					console.log(e.src);
					e.src = e.src.replace(/\_mt.jpg/g, '_t.jpg');
				});
			}
		}
		, 1500);
		*/
	
	}else if (window.location.pathname.startsWith("/collection/user") || window.location.pathname.startsWith("/browse/boardgame") || window.location.pathname.startsWith("/geeksearch.php")) {
		
		//console.log("list view");
		
		// Inject the SD, and suggested player counts to lists for easy access
		jQuery("#collectionitems > tbody > tr").each(function (idx, item) {
			// Insert the header col for suggested player counts
			if(idx == 0){
				var suggestedPlayersCell = item.insertCell(3);
				//suggestedPlayersCell.css('text-align', 'center');
				suggestedPlayersCell.innerHTML = "<b>Suggested Player Counts</b>";
			}
			
			if (idx > 0) {
				// With or without data, a new column needs to be added
				var suggestedPlayersCell = item.insertCell(3);
				
				var link = jQuery(item).find(".collection_objectname a");
				var boardGameID = link.attr('href').split('/')[2];
				var boardGameInfo = GM_getValue(boardGameID);
				
				if (typeof boardGameInfo !== "undefined" && boardGameInfo !== null) {
					boardGameInfo =  JSON.parse(boardGameInfo);
					
					// Insert the SD into the rating
					var ratingIndex = 0;
					if(typeof boardGameInfo.standardDeviation !== 'undefined' && boardGameInfo.standardDeviation != null){
						if (window.location.pathname.startsWith("/browse/boardgame")) {
							ratingIndex = 1;
						}
						var rating = jQuery(item).children(".collection_bggrating:eq(" + ratingIndex + ")");
						rating.html(rating.html() + "<br/>(SD " + boardGameInfo.standardDeviation.toFixed(2) + ")");
					}
					
					// Insert data into the new col for the suggested player count
					if(typeof boardGameInfo.suggestedPlayerCount !== 'undefined' && boardGameInfo.suggestedPlayerCount != null){
						
						//console.log(boardGameInfo);
						//console.log(boardGameInfo.suggestedPlayerCount);
						for (var i = 1, len = Math.min(boardGameInfo.suggestedPlayerCount.length, 9); i < len; i++) {
							
							if(i < 9 || boardGameInfo.suggestedPlayerCount.length <=9)
							{
								var item = boardGameInfo.suggestedPlayerCount[i];
								var total = item.best + item.good + item.bad;
								//console.log(total);
								if(total > 0 && item.best / total >= 0.5){
									suggestedPlayersCell.innerHTML = suggestedPlayersCell.innerHTML + "<span style='color:green; font-size: x-large; font-weight: bold'>" + i + "</span>";
								}
								else if(total > 0 && item.good / total >= 0.5){
									suggestedPlayersCell.innerHTML = suggestedPlayersCell.innerHTML + "<span style='color:#aa0; font-size: large; font-weight: bold'>" + i + "</span>";
								}
								else if(total > 0 && item.bad / total >= 0.5){
									suggestedPlayersCell.innerHTML = suggestedPlayersCell.innerHTML + "<span style='color:red; font-size: small; font-weight: bold'>" + i + "</span>";
								}
								else{
									suggestedPlayersCell.innerHTML = suggestedPlayersCell.innerHTML + "<span style='font-size: medium; font-weight: bold'>" + i + "</span>";
								}
							}
							
							//suggestedPlayersCell.html(suggestedPlayersCell.html() + item);
						}
						
						
						// Handle games with massive player counts
						if(boardGameInfo.suggestedPlayerCount.length > 9)
						{
							//console.log('9+');
							var sumBestOverNine = 0;
							var sumGoodOverNine = 0;
							var sumBadOverNine = 0;
							var totalVotesOverNine = 0;
							var countOverNine = 0;
							
							var asdf = boardGameInfo.suggestedPlayerCount.length;
							for (var j = 9; j < asdf; j++) {
								var item = boardGameInfo.suggestedPlayerCount[j];
								sumBestOverNine += item.best;
								sumGoodOverNine += item.good;
								sumBadOverNine += item.bad;
								totalVotesOverNine += item.best + item.good + item.bad;
								countOverNine++;
							}
							
							if(totalVotesOverNine > 0 && sumBestOverNine / totalVotesOverNine >= 0.5){
								suggestedPlayersCell.innerHTML = suggestedPlayersCell.innerHTML + "<span style='color:green; font-size: x-large; font-weight: bold'>9+</span>";
							}
							else if(totalVotesOverNine > 0 && sumGoodOverNine / totalVotesOverNine >= 0.5){
								suggestedPlayersCell.innerHTML = suggestedPlayersCell.innerHTML + "<span style='color:#aa0; font-size: large; font-weight: bold'>9+</span>";
							}
							else if(totalVotesOverNine > 0 && sumBadOverNine / totalVotesOverNine >= 0.5){
								suggestedPlayersCell.innerHTML = suggestedPlayersCell.innerHTML + "<span style='color:red; font-size: small; font-weight: bold'>9+</span>";
							}
							else{
								suggestedPlayersCell.innerHTML = suggestedPlayersCell.innerHTML + "<span style='font-size: medium; font-weight: bold'>9+</span>";
							}
						}
						
					}
					else
					{
						suggestedPlayersCell.innerHTML = "View game page to load data";
					}
					
				}
				else
				{
					// No cached data available for this game.
					suggestedPlayersCell.innerHTML = "View game page to load data";
				}
			}
		});
	
	
	}else if (window.location.pathname.startsWith("/play/details/")) {
		// Inject a button to copy plays
		jQuery('#main_content').append('<a class="comment_button" id="copyPlay">Copy Play</a><div id="copyPlayResults"></div>');
		jQuery('#copyPlay')[0].addEventListener("click", function(){ copyPlay(); } );
		
	}
}

// angular.element('.game').inheritedData().$geekitemController.geekitem.data

// Chrome will use another content script to initialise the script.
// TODO: Make sure the Chrome plugin doesn't do its own init before the AJAX has finished
// Look into using geekitemctrl.geekitem.loaded
if(boardgamePage.isBetaUI()){
	var waitTilPageLoaded = setInterval ( function () {
			var gameElement = unsafeWindow.angular.element('.game');
			if(gameElement){
				var loaded = gameElement.inheritedData().$scope.geekitemctrl.geekitem.loaded;
				
				//var node = jQuery('button[item-poll-button="numplayers"]');
				//if(node && node.length > 0){
				if(loaded){
					// console.log('Loaded');
					clearInterval (waitTilPageLoaded);
					init();
				}
			}
		},
		300
	);
}else{
	init();
}

