// ==UserScript==
// @name        BetterBGG
// @namespace   jcpmcdonald
// @description Better BoardGameGeek
// @include     /^http(s)?://(www\.)?boardgamegeek\.com/.*$/
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js
// @version     5.0
// @updateURL   https://bitbucket.org/jcpmcdonald/betterbgg/downloads/BetterBGG.meta.js
// @downloadURL https://bitbucket.org/jcpmcdonald/betterbgg/downloads/BetterBGG.user.js
// @resource    configHtml http://jcpmcdonald.com/GreaseMonkey/BetterBGG.config.html
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_listValues
// @grant       GM_xmlhttpRequest
// ==/UserScript==
